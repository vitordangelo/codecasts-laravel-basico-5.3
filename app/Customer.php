<?php

namespace Bird;

use Illuminate\Database\Eloquent\Model;
use Bird\Purchase;

class Customer extends Model
{
    protected $fillable = ['name', 'birthdate', 'special_customer', 'city', 'state'];

    protected $casts = [
        'special_customer' => 'boolean',
        'birthdate' => 'date',

    ];

        public function purchases()
    {
        return $this->hasMany(Purchases::class);
    }

}
