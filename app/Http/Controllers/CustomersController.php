<?php

namespace Bird\Http\Controllers;

use Illuminate\Http\Request;
use Bird\Customer;
use DB;

class CustomersController extends Controller
{

    public function index()
    {
        $customers = Customer::all();
        return view('customers.index')->with(compact('customers'));
    }


    public function create()
    {

    }


    public function store(Request $request)
    {
        $nome = $request->nome;
        $email = $request->email;
        $users = DB::table('customers')
                ->where('name', '=', $nome)
                ->get();
        dd($request->toArray());
    }


    public function show($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('admin/clientes');
    }


    public function edit($id)
    {
        $customer = Customer::find($id);
        $customersForSelect = Customer::groupBy('state')->get(['state'])->pluck('state', 'state');
        return view('customers.edit')->with(compact('customer', 'customersForSelect'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required'
        ]);

        $input = $request->only('name', 'special_customer', 'city', 'state');
        $input['special_customer'] = isset($input['special_customer']);
        $customer = Customer::find($id);
        $customer->fill($input);
        $customer->save();
        return redirect()->route('clientes.edit', $id)->with(['success' => 'Cliente salvo com sucesso!']);
    }


    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        dd($customer);
    }
}
