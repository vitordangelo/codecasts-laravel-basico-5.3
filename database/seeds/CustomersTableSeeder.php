<?php

use Illuminate\Database\Seeder;
use Bird\Customer;

class CustomersTableSeeder extends Seeder
{
    public function run()
    {
        Customer::truncate();
        
        factory(Customer::class, 50)->create();
    }
}
