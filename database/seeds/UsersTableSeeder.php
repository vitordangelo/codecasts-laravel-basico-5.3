<?php

use Illuminate\Database\Seeder;
use Bird\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::truncate();

        $user = new User();
        $user->name = 'Vitor';
        $user->email = 'vitordangelo@gmail.com';
        $user->password = bcrypt(123456);
        $user->save();

        factory(User::class, 50)->create();
    }
}
