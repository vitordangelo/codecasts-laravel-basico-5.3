@extends('layouts.main')

@section('page-title')
    Criar novo Cliente
@stop

@section('content')
    {!! Form::open(['url' => 'foo/bar']) !!}
        @include('customers.partials.form')
    {!! Form::close()!!}
@stop
