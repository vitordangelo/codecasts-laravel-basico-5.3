<?php

Route::group(['prefix' => 'admin'], function() {
    Route::resource('clientes', 'CustomersController');
});

Route::get('/', function () {
    $page = Bird\User::paginate(10);

    return Response::json([
        'draw' => $page->currentPage(),
        'recordsTotal' => $page->total(),
        'recordsFiltered' => $page->total(),
        'data' => $page = Bird\User::paginate(10)->toArray()['data']
    ]);
});
